<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>B. T a m u</title>
    <script type="text/javascript" src="jvs.js"></script>
    <style type="text/css">
          *{
    margin: 0px;
    padding: 0px;
  }

  body{

  font-family: Tahoma, Geneva, sans-serif;
  background-image: linear-gradient(rgba(0,0,0,0.6),rgba(0,0,0,0.6)),url(xui.jpg);
  background-size: cover;
  height: 100vh;
  background-position: center;
  }

  body nav{

    width: auto;
    padding: 10px;
    background-color: #008b8b;
    height: 5em;

  }

  body nav ul{

    float: left;
    animation-duration: 1s;
    position: relative;
  }

  .aktif{

    background-color: black;
    color: #008b8b;
    border-radius: 5px;
    padding-bottom: 2px;
    padding-left: 3px;
    padding-right: 3px;
    padding-bottom: 35px;
  }

  ul li{
    display: inline-block;
    position: relative;
    font-size: 50px;
    margin-top: 15px;
    padding-right: 50px;
  }

  ul .log1{
    padding-right: 0px;
  }

  ul .log{
    font-size: 20px;
    margin-left: 600px;
    background-color: black;
    border-radius: 5px;
    padding: 10px;
    color: #008b8b;
    border: 0px;
  }

  .log:hover{
  color: black;
  background-color: #708090;
  text-decoration: blink;
  }

  a {
  text-decoration: none;
  font-size: 30px;
  color: black;
  }

  a:hover{
  background-color: black;
  border-radius: 5px;
  padding-bottom: 3px;
  color: #008b8b;
  }

  .daftar {
  background: rgba(119,136,153,0.8);
  box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.2);
  width: 560px;
  height: auto;
  margin-bottom: 50px;
  font-family: junction;
  font-size: 18px;
  }
  .isi a{
    font-size: 20px;
    color: yellow;
  }

  .isi a:hover{
    color: lime;
    background-color: inherit;
  }

  .klik{
    font-size: 20px;
    color: black;
  }

  h1{
  margin-top: 10px;
  margin-bottom: 10px;
  font-size: 50px;
  text-align:center;
  font-family: Brush Script MT;
  color: #b0c4de;
  margin-top: 20px;
  }

  .footer{
      width: 100%;
      height: 20px;
      padding: 5px;
      text-align: center;
      color: black;
      background-color: #008b8b;
      font-family: 'Roboto';
      position: button;
      bottom: 0px;
  }
</style>
  </head>
  <body>

    <nav>
        <ul>
            <li><a href="home.php">Home</a></li>
            <li><a href="story.php">History</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
            <li><a class="aktif" href="gbook.php">Buku Tamu</a></li>
            <li class="log1"><input type="button" class="log" value="Logout" onclick="logout()"></li>
        </ul>
    </nav>

    <h1>Daftar tamu</h1>

    <center>
    <div class="daftar">
      <?php
          $fp = fopen("bukutamu.txt","r");
          echo "<table border=0";

          while ($isi = fgets($fp,80)){

            $pisah = explode("|",$isi);

            echo "<tr><td> Nama Lengkap </td><td>: $pisah[0]</td></tr>";
            echo "<tr><td> Alamat </td><td>: $pisah[1]</td></tr>";
            echo "<tr><td> Email </td><td>: $pisah[2]</td></tr>";
            echo "<tr><td> Jenis Kelamin </td><td>: $pisah[3]</td></tr>";
            echo "<tr><td> Komentar </td><td>: $pisah[4]</td></tr> <tr><td> &nbsp; <hr></td> <td> &nbsp; <hr></td></tr>";
          }

          echo "</table>";
          echo "<br>";

          echo "<div class=isi>";
          echo "<p class=klik>Isi Formulir Buku Tamu? <a href=gbook.php>Klik Disini!</a></p>";
          echo "</div>";
      ?>
    </div>
  </center><br>

  <div class="footer">
      <center><b>Copyright @panjisaputra</b></center>
  </div>
  </body>
</html>
