<!DOCTYPE html>
<html>
<head>
    <title>A b o u t</title>
    <script type="text/javascript" src="jvs.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
*{
    margin: 0px;
    padding: 0px;
}

body{
  font-family: Tahoma, Geneva, sans-serif;
  background-image: linear-gradient(rgba(0,0,0,0.6),rgba(0,0,0,0.6)),url(xui.jpg);
  background-size: cover;
  height: 100vh;
  background-position: center;
}

body nav{
    width: auto;
    padding: 10px;
    background-color: #008b8b;
    height: 5em;
}

body nav ul{
    float: left;
    animation-duration: 1s;
    position: relative;
}

.aktif{

    background-color: black;
    color: #008b8b;
    border-radius: 5px;
    padding-bottom: 2px;
    padding-left: 3px;
    padding-right: 3px;
    padding-bottom: 35px;
}

ul li{
    display: inline-block;
    position: relative;
    font-size: 50px;
    margin-top: 15px;
    padding-right: 50px;
}

ul .log1{
    padding-right: 0px;
}

ul .log{
    font-size: 20px;
    margin-left: 600px;
    background-color: black;
    border-radius: 5px;
    padding: 10px;
    color: #008b8b;
    border: 0px;
}

.log:hover{
  color: black;
  background-color: #708090;
  text-decoration: blink;
}

a {
  text-decoration: none;
  font-size: 30px;
  color: black;
}

a:hover{
  background-color: black;
  border-radius: 5px;
  padding-bottom: 3px;
  color: #008b8b;
}

.card {
  background: rgba(119,136,153,0.8);
  box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.2);
  width: 480px;
  height: 600px;
  margin: auto;
  text-align: center;
  font-family: junction;
}

.title {
  color: black;
  font-size: 16px;
  margin-bottom: 10px;
}

h1{
  margin-top: 10px;
  margin-bottom: 10px;
  font-size: 40px;
  text-align:center;
  font-family: Brush Script MT;
  color: #b0c4de;
  margin-top: 20px;
}

h2{
  font-size: 35px;
  text-align: center;
  font-family: junction;
  margin-bottom: 15px;
}

h3{
  margin-bottom: 15px;
}

img {
      width: 50%;
      height:180px;
      margin-bottom: 7px;
      border-radius: 50%;
      margin-top: 20px;
    }

    .footer{
        width: 100%;
        height: 20px;
        padding: 5px;
        text-align: center;
        color: black;
        background-color: #008b8b;
        font-family: 'Roboto';
        bottom: 0px;
    }

    </style>
</head>
<body>
    <nav>
        <ul>
            <li><a href="home.php">Home</a></li>
            <li><a href="story.php">History</a></li>
            <li><a class="aktif" href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
            <li><a href="gbook.php">Buku Tamu</a></li>
            <li class="log1"><input type="button" class="log" value="Logout" onclick="logout()"></li>
        </ul>
    </nav>

<center>
<h1>Biodata</h1>

<div class="card">
  <img src="xx.jpeg">
  <h2>Nurkholis Panji Saputra</h2>
  <h3>10 April 1999</h3>
  <p class="title"><b>NIM : 1900018372</b></p>
  <p class="title"><b>Teknik Informatika UAD</b></p>
  <p class="title"><b>My hobby is playing football</b></p>
  <p style="font-size: 21px"><b>Rejosari, Jogotirto, Berbah, Sleman, Yogyakarta 55573</b></p>
  <div style="margin: 20px 0;">
    <a href="https://twitter.com/panjisuuu"><i class="fa fa-twitter"></i></a>
    <a href="https://www.instagram.com/panjisaputraa_/"><i class="fa fa-instagram"></i></a>
    <a href="https://web.facebook.com/panjhiee/"><i class="fa fa-facebook"></i></a>
  </div>
  <p class="title">
        <?php
            function tampil(){
            echo "<b><br>Warna kesukaan saya : <br>";
        }

        $index=array("Hijau","Kuning","Hitam","Merah","Biru");
        tampil();
        echo $index[2];
        ?>
      </p>
</div>
<br><br>
<div class="footer">
    <center><b>Copyright @panjisaputra</b></center>
</div>

</body>
</html>
