<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>B. T a m u</title>
    <script type="text/javascript" src="jvs.js"></script>
    <style type="text/css">
          *{
    margin: 0px;
    padding: 0px;
}

body{

  font-family: Tahoma, Geneva, sans-serif;
  background-image: linear-gradient(rgba(0,0,0,0.6),rgba(0,0,0,0.6)),url(xui.jpg);
  background-size: cover;
  height: 100%;
  background-position: center;
}

body nav{

    width: auto;
    padding: 10px;
    background-color: #008b8b;
    height: 5em;

}

body nav ul{

    float: left;
    animation-duration: 1s;
    position: relative;
}

.aktif{

    background-color: black;
    color: #008b8b;
    border-radius: 5px;
    padding-bottom: 2px;
    padding-left: 3px;
    padding-right: 3px;
    padding-bottom: 35px;
}

ul li{
    display: inline-block;
    position: relative;
    font-size: 50px;
    margin-top: 15px;
    padding-right: 50px;
}

ul .log1{
    padding-right: 0px;
}

ul .log{
    font-size: 20px;
    margin-left: 600px;
    background-color: black;
    border-radius: 5px;
    padding: 10px;
    color: #008b8b;
    border: 0px;
}

.log:hover{
  color: black;
  background-color: #708090;
  text-decoration: blink;
}

a {
  text-decoration: none;
  font-size: 30px;
  color: black;
}

a:hover{
  background-color: black;
  border-radius: 5px;
  padding-bottom: 3px;
  color: #008b8b;
}

.kotak {
  background: rgba(119,136,153,0.8);
  box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.2);
  width: 560px;
  height: 320px;
  padding: 20px;
  font-family: junction;
}

tr{
  font-family: Tahoma, Geneva, sans-serif;
  font-size: 20px;
}
.kirm{
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	text-transform: uppercase;
	outline: 0;
	background: #008b8b;
	width: 75%;
  float: right;
	border: 0;
	padding: 8px;
	color: black;
	cursor: pointer;
	border-radius: 10px;
}

.btl{
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	text-transform: uppercase;
	outline: 0;
	background: silver;
	width: 32%;
	border: 0;
	padding: 8px;
	color: black;
	cursor: pointer;
	border-radius: 10px;
}

.kirm:hover{
	color: yellow;
	background-color: black;
}

.btl:hover{
	color: yellow;
	background-color: black;
}

.liat a{
  font-size: 20px;
  color: yellow;
}

.liat a:hover{
  color: lime;
  background-color: inherit;
}

.klik{
  font-size: 20px;
  color: black;
}
h1{
  margin-top: 10px;
  margin-bottom: 10px;
  font-size: 40px;
  text-align:center;
  font-family: Brush Script MT;
  color: #b0c4de;
  margin-top: 20px;
}

.footer{
    width: 100%;
    height: 20px;
    padding: 5px;
    text-align: center;
    color: black;
    background-color: #008b8b;
    font-family: 'Roboto';
    position: absolute;
    bottom: 0px;
}

    </style>
  </head>
  <body>
    <nav>
        <ul>
            <li><a href="home.php">Home</a></li>
            <li><a href="story.php">History</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
            <li><a class="aktif" href="gbook.php">Buku Tamu</a></li>
            <li class="log1"><input type="button" class="log" value="Logout" onclick="logout()"></li>
        </ul>
    </nav>

    <h1>Buku tamu</h1>

    <center>
    <form class="kotak" action="proses.php" method="post">
      <table>
        <tr>
          <td>Nama Lengkap</td>
          <td><input type="text" name="nama" id="nama" required></td>
        </tr>

        <tr>
          <td>Alamat</td>
          <td><input type="text" name="alamat" id="alamat" required></td>
        </tr>

        <tr>
          <td>E-Mail</td>
          <td><input type="email" name="mail" id="mail" required></td>
        </tr>

        <tr>
          <td>Jenis Kelamin </td>
          <td><select class="" name="jenis" id="jenis" required>
            <option value="Laki - Laki" name="jenis">Laki Laki</option>
            <option value="Perempuan" name="jenis">Perempuan</option>
          </select></td>
        </tr>

        <tr>
          <td>Komentar </td>
          <td><textarea name="komen" rows="4" cols="40" id="komen" required></textarea></td>
        </tr>

        <tr>
          <td><input type="submit" name="submit1" class="kirm" value="KIRIM"></td>
          <td> <input type="reset" name="submit2" class="btl" value="BATAL"></td>
        </tr>

      </table><br><br>

      <div class="liat">
          <p class="klik">Daftar Tamu? <a href="lihat.php">Klik Disini!</a></p>
      </div>

    </form></center>

    <div class="footer">
        <center><b>Copyright @panjisaputra</b></center>
    </div>


  </body>
</html>
