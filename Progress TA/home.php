<!DOCTYPE html>
<html>
<head>
  <title>H o m e</title>
  <script type="text/javascript" src="jvs.js"></script>
  <style type="text/css">
  *{
    margin: 0px;
    padding: 0px;
}

body{
  font-family: Tahoma, Geneva, sans-serif;
  background-image: linear-gradient(rgba(0,0,0,0.6),rgba(0,0,0,0.6)),url(xui.jpg);
  background-size: cover;
  height: 100vh;
  background-position: center;
}

body nav{
    width: auto;
    padding: 10px;
    background-color: #008b8b;
    height: 5em;
}

body nav ul{
    float: left;
    animation-duration: 1s;
    position: relative;
}

.aktif{

    background-color: black;
    color: #008b8b;
    border-radius: 5px;
    padding-bottom: 2px;
    padding-left: 3px;
    padding-right: 3px;
    padding-bottom: 35px;
}

ul li{
    display: inline-block;
    position: relative;
    font-size: 50px;
    margin-top: 15px;
    padding-right: 50px;
}

ul .log1{
    padding-right: 0px;
}

ul .log{
    font-size: 20px;
    margin-left: 600px;
    background-color: black;
    border-radius: 5px;
    padding: 10px;
    color: #008b8b;
    border: 0px;
}

.log:hover{
  color: black;
  background-color: #708090;
  text-decoration: blink;
}

a {
  text-decoration: none;
  font-size: 30px;
  color: black;
}

a:hover{
  background-color: black;
  border-radius: 5px;
  padding-bottom: 3px;
  color: #008b8b;
}


h1{
  margin-top: 170px;
  margin-bottom: 50px;
  font-size: 120px;
  text-align:center;
  font-family: Tahoma, Geneva, sans-serif;
  color: #b0c4de;

}

h5{
  font-size: 20px;
  color: #b0c4de;
}
.waktu{
  font-size: 38px;
  margin-bottom: 10px;
  color: yellow;
  text-align: center;
}
.footer{
    width: 100%;
    height: 20px;
    padding: 5px;
    text-align: center;
    color: black;
    background-color: #008b8b;
    font-family: 'Roboto';
    position: absolute;
    bottom: 0px;
}
</style>
</head>
<body>
  <nav>
        <ul>
            <li><a class="aktif" href="home.php">Home</a></li>
            <li><a href="story.php">History</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
            <li><a href="gbook.php">Buku Tamu</a></li>
            <li class="log1"><input type="button" class="log" value="Logout" onclick="logout()"></li>
        </ul>
    </nav>
    <div class="view">
    <?php
  			$filecounter = "counter.txt";
  			$fl = fopen($filecounter,"r+");

  			$hit = fread($fl,filesize($filecounter));

  			echo("<center><table width =250 align=center border=1 cellspaacing =0 cellpadding=0 bordercolor=black><tr>");
  			echo("<td width=250 valign=middle align=center>");
  			echo("<font face=verdana size=2 color=yellow><b>");
  			echo("Total Halaman dilihat : ");
  			echo($hit);
  			echo("</b></font>");
  			echo("</td>");
  			echo("</tr></table></center>");
  			fclose($fl);

  			$fl = fopen($filecounter, "w+");

  			$hit = $hit+1;

  			fwrite($fl, $hit, strlen($hit));

  			fclose($fl);
  		 ?>

     </div>

  <h1>WELLCOME ...</h1>

  <div class="waktu">
  <?php
  date_default_timezone_set('Asia/Jakarta');
  $t = date("H:i");
  echo "<h5>Sekarang Pukul  " . $t. " WIB</h5>";

  if ($t < "10") {
    echo "Good Morning!";
  } elseif ($t < "20") {
    echo "Have a Good Day!";
  } else {
    echo "Have a Good Night!";
  }
  ?>

  </div>
  <div class="footer">
      <center><b>Copyright @panjisaputra</b></center>
  </div>

</body>
</html>
