<!DOCTYPE html>
<html>
<head>
	<title>S t o r y</title>
    <script type="text/javascript" src="jvs.js"></script>
	<style type="text/css">
  *{
    margin: 0px;
    padding: 0px;
}

body{

  font-family: Tahoma, Geneva, sans-serif;
  background-image: linear-gradient(rgba(0,0,0,0.6),rgba(0,0,0,0.6)),url(xui.jpg);
  background-size: cover;
  height: 100vh;
  background-position: center;
}

body nav{

    width: auto;
    padding: 10px;
    background-color: #008b8b;
    height: 5em;

}

body nav ul{

    float: left;
    animation-duration: 1s;
    position: relative;
}

.aktif{

    background-color: black;
    color: #008b8b;
    border-radius: 5px;
    padding-bottom: 2px;
    padding-left: 3px;
    padding-right: 3px;
    padding-bottom: 35px;
}

ul li{
    display: inline-block;
    position: relative;
    font-size: 50px;
    margin-top: 15px;
    padding-right: 50px;
}

ul .log1{
    padding-right: 0px;
}

ul .log{
    font-size: 20px;
    margin-left: 600px;
    background-color: black;
    border-radius: 5px;
    padding: 10px;
    color: #008b8b;
    border: 0px;
}

.log:hover{
  color: black;
  background-color: #708090;
  text-decoration: blink;
}

a {
  text-decoration: none;
  font-size: 30px;
  color: black;
}

a:hover{
  background-color: black;
  border-radius: 5px;
  padding-bottom: 3px;
  color: #008b8b;
}

.cv {
  background: rgba(119,136,153,0.8);
  box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.2);
  width: 480px;
  height: 400px;
  margin: auto;
  text-align: center;
  font-family: junction;
}

h1{
  margin-top: 10px;
  margin-bottom: 10px;
  font-size: 40px;
  text-align:center;
  font-family: Brush Script MT;
  color: #b0c4de;
  margin-top: 20px;
}
h2{
	color: yellow;
}

h4{
	text-align: left;
	margin-left: 20px;
}
.footer{
    width: 100%;
    height: 20px;
    padding: 5px;
    text-align: center;
    color: black;
    background-color: #008b8b;
    font-family: 'Roboto';
    position: absolute;
    bottom: 0px;
}
</style>
</head>
<body>
	<nav>
        <ul>
            <li><a href="home.php">Home</a></li>
            <li><a class="aktif" href="story.php">History</a></li>
            <li><a href="about.html">About</a></li>
						<li><a href="contact.php">Contact</a></li>
						<li><a href="gbook.php">Buku Tamu</a></li>
            <li class="log1"><input type="button" class="log" value="Logout" onclick="logout()"></li>
        </ul>
    </nav>

    <h1>Panji Saputra History</h1>

    <div class="cv">
			<?php

			echo "<br><br>";
      echo "<h2>*PENDIDIKAN*<h2><br>";
			echo "<h4>2004-2005 => TK ABA JOGOMANGSAN<br>";
			echo "<h4>2005-2011 => SD MUHAMMADYAH BULU<br>";
			echo "<h4>2011-2014 => SMP N 1 PIYUNGAN<br>";
			echo "<h4>2014-2017 => SMK N 2 YOGYAKARTA<h4><br><br>";
			echo "<h2>*PEKERJAAN*<h2><br>";
			echo "<h4>2017 (3 Bulan) PT. MAYORA INDAH Tbk. => OPERATOR CNC<br>";
			echo "<h4>2017-2018 (9 Bulan) PT. YPTI => OPERATOR CNC<br>";

       ?>
    </div>

		<div class="footer">
				<center><b>Copyright @panjisaputra</b></center>
		</div>

</body>
</html>
