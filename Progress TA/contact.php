<!DOCTYPE HTML>
<html>
<head>
<style>
*{
  margin: 0px;
  padding: 0px;
}

body{
font-family: Tahoma, Geneva, sans-serif;
background-image: linear-gradient(rgba(0,0,0,0.6),rgba(0,0,0,0.6)),url(xui.jpg);
background-size: cover;
height: 100vh;
background-position: center;
}

body nav{
  width: auto;
  padding: 10px;
  background-color: #008b8b;
  height: 5em;
}

body nav ul{
  float: left;
  animation-duration: 1s;
  position: relative;
}

.aktif{

  background-color: black;
  color: #008b8b;
  border-radius: 5px;
  padding-bottom: 2px;
  padding-left: 3px;
  padding-right: 3px;
  padding-bottom: 35px;
}

ul li{
  display: inline-block;
  position: relative;
  font-size: 50px;
  margin-top: 15px;
  padding-right: 50px;
}

ul .log1{
  padding-right: 0px;
}

ul .log{
  font-size: 20px;
  margin-left: 600px;
  background-color: black;
  border-radius: 5px;
  padding: 10px;
  color: #008b8b;
  border: 0px;
}

.log:hover{
color: black;
background-color: #708090;
text-decoration: blink;
}

a {
text-decoration: none;
font-size: 30px;
color: black;
}

a:hover{
background-color: black;
border-radius: 5px;
padding-bottom: 3px;
color: #008b8b;
}

.form{
  background: rgba(119,136,153,0.8);
  box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.2);
  width: 480px;
  height: 500px;
  margin: auto;
  text-align: left;
  font-family: junction;
}
h2{
  margin-top: 10px;
  margin-bottom: 10px;
  font-size: 40px;
  text-align: center;
  font-family: Tahoma, Geneva, sans-serif;
  color: #b0c4de;
  margin-top: 20px;
}
.error {color: yellow;
}
input[type=submit]{
  font-family: Tahoma, Geneva, sans-serif;
	font-size: 15px;
	text-transform: uppercase;
	outline: 0;
	background: #008b8b;
  margin-left: 110px;
	width: 50%;
	border: 0;
	padding: 8px;
	color: black;
	cursor: pointer;
	border-radius: 10px;
}

input[type=submit]:hover{
	color: yellow;
	background-color: black;
}

footer{
      padding: 5px;
      text-align: center;
      color: black;
      background-color: #008b8b;
      font-family: 'Roboto';
    }

</style>
</head>
<body>

<?php

$nameErr = $emailErr = $genderErr = $websiteErr = "";
$name = $email = $gender = $comment = $website = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } else {
    $name = test_input($_POST["name"]);

    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed";
    }
  }

  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format";
    }
  }

  if (empty($_POST["website"])) {
    $website = "";
  } else {
    $website = test_input($_POST["website"]);

    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
      $websiteErr = "Invalid URL";
    }
  }

  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<nav>
      <ul>
          <li><a href="home.php">Home</a></li>
          <li><a href="story.php">History</a></li>
          <li><a href="about.php">About</a></li>
          <li><a class="aktif" href="contact.php">Contact</a></li>
          <li><a href="gbook.php">Buku Tamu</a></li>
          <li class="log1"><input type="button" class="log" value="Logout" onclick="logout()"></li>
      </ul>
  </nav>

<h2>Contact Form</h2>
<div class="form">
  <br>
<p><span class="error">* required field</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
  Name      : <input type="text" name="name" value="<?php echo $name;?>">
  <span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  E-mail    : <input type="text" name="email" value="<?php echo $email;?>">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
  Website    : <input type="text" name="website" value="<?php echo $website;?>">
  <span class="error"><?php echo $websiteErr;?></span>
  <br><br>
  Comment : <textarea name="comment" rows="5" cols="40"><?php echo $comment;?></textarea>
  <br><br>
  Gender     :
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?> value="female">Female
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?> value="male">Male
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="other") echo "checked";?> value="other">Other
  <span class="error">* <?php echo $genderErr;?></span>
  <br><br>
  <input type="submit" name="submit" value="Submit">
</form>

<?php
echo "<br><h3>Your Input:</h3>";
echo $name;
echo "<br>";
echo $email;
echo "<br>";
echo $website;
echo "<br>";
echo $comment;
echo "<br>";
echo $gender;
?>
</div>
<br><br>
<footer>
    <p><b>Copyright @panjisaputra</b></p>
  </footer>

</body>
</html>
